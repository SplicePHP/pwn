<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'AdminTheme' => $baseDir . '/plugins/AdminTheme/',
        'Asset' => $baseDir . '/vendor/splicephp/asset/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Gizmo' => $baseDir . '/vendor/splicephp/gizmo/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Pwn' => $baseDir . '/plugins/Pwn/',
        'System' => $baseDir . '/plugins/System/'
    ]
];
