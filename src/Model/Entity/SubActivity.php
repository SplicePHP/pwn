<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SubActivity Entity.
 */
class SubActivity extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'activity_id' => true,
		'activity' => true,
		'target' => true,
		'activity' => true,
		'dates' => true,
		'means_of_verifications' => true,
	];

}
