<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArticleCategory Entity.
 */
class ArticleCategory extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'alias' => true,
		'title' => true,
		'description' => true,
		'parent_id' => true,
		'lft' => true,
		'rght' => true,
		'created' => true,
		'updated' => true,
		'child_article_categories' => true,
		'parent_article_category' => true,
		'articles' => true,
	];

}
