<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Impact Entity.
 */
class Impact extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'parent_id' => true,
		'lft' => true,
		'rght' => true,
		'impact_statement' => true,
		'child_impacts' => true,
		'impacts_indicators' => true,
		'parent_impact' => true,
		'objectives' => true,
	];

}
