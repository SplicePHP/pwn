<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkplanOutcomesIndicator Entity.
 */
class WorkplanOutcomesIndicator extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'workplan_outcome_id' => true,
		'indicator_id' => true,
		'workplan_outcome' => true,
	];

}
