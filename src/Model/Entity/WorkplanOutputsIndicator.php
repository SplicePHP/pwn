<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkplanOutputsIndicator Entity.
 */
class WorkplanOutputsIndicator extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'workplan_output_id' => true,
		'indicator_id' => true,
		'workplan_output' => true,
	];

}
