<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactInformation Entity.
 */
class ContactInformation extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'alias' => true,
		'name' => true,
		'surname' => true,
		'tel' => true,
		'mobile' => true,
		'fax' => true,
		'street_address' => true,
		'postal_address' => true,
		'donors' => true,
	];

}
