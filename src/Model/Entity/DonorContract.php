<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DonorContract Entity.
 */
class DonorContract extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'contract_type_id' => true,
		'donor_id' => true,
		'total_amount' => true,
		'start_date' => true,
		'end_date' => true,
		'other' => true,
		'contract_type' => true,
		'donor' => true,
		'attachments' => true,
	];

}
