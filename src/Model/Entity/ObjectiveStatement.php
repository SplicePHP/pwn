<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ObjectiveStatement Entity.
 */
class ObjectiveStatement extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'objective_id' => true,
		'statement' => true,
		'objective' => true,
	];

}
