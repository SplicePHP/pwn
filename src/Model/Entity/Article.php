<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity.
 */
class Article extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'user_id' => true,
		'published' => true,
		'alias' => true,
		'title' => true,
		'caption' => true,
		'body' => true,
		'meta_description' => true,
		'meta_keys' => true,
		'created' => true,
		'updated' => true,
		'weight' => true,
		'user' => true,
		'article_categories' => true,
	];

}
