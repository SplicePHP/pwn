<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Date Entity.
 */
class Date extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'begin' => true,
		'end' => true,
		'info' => true,
		'activities' => true,
		'actuals' => true,
		'sub_activities' => true,
	];

}
