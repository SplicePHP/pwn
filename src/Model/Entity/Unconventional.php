<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Unconventional Entity.
 */
class Unconventional extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'users_id' => true,
		'title' => true,
	];

}
