<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WidgetAttribute Entity.
 */
class WidgetAttribute extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'widget_id' => true,
		'title' => true,
		'widget' => true,
	];

}
