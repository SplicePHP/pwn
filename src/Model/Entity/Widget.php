<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Widget Entity.
 */
class Widget extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'widget_parent_id' => true,
		'alias' => true,
		'title' => true,
		'type' => true,
		'prefix' => true,
		'widget_attributes' => true,
		'widget_parent' => true,
		'roles' => true,
	];

}
