<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Responsibility Entity.
 */
class Responsibility extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'title' => true,
		'users' => true,
		'workplan_outputs' => true,
	];

}
