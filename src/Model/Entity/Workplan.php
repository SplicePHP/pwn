<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Workplan Entity.
 */
class Workplan extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'donor_id' => true,
		'objectives' => true,
		'workplan_outcomes' => true,
		'workplan_outputs' => true,
		'donor' => true,
		'partners' => true,
	];

}
