<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActivityStatus Entity.
 */
class ActivityStatus extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'status' => true,
		'actuals' => true,
	];

}
