<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FormActivityReport Entity.
 */
class FormActivityReport extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'name' => true,
		'date_of_activity' => true,
		'location_id' => true,
		'activity_type_id' => true,
		'number_of_people' => true,
		'women_0_12' => true,
		'women_13_18' => true,
		'women_19_25' => true,
		'women_26_35' => true,
		'women_36_60' => true,
		'women_61_#' => true,
		'men_0_12' => true,
		'men_13_18' => true,
		'men_19_25' => true,
		'men_26_35' => true,
		'men_36_60' => true,
		'men_61_#' => true,
		'lgbti_0_12' => true,
		'lgbti_13_18' => true,
		'lgbti_19_25' => true,
		'lgbti_26_35' => true,
		'lgbti_36_60' => true,
		'lgbti_60_#' => true,
		'other_ngos' => true,
		'other_partners' => true,
		'comments' => true,
		'date_of_next_activity' => true,
		'attendance_register' => true,
		'other_documents' => true,
		'location' => true,
		'activity_type' => true,
	];

}
