<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MeansOfVerification Entity.
 */
class MeansOfVerification extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'title' => true,
		'activities' => true,
		'sub_activities' => true,
	];

}
