<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Input Entity.
 */
class Input extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'title' => true,
		'activities' => true,
	];

}
