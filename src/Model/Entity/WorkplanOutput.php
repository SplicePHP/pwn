<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkplanOutput Entity.
 */
class WorkplanOutput extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'workplan_id' => true,
		'output_id' => true,
		'output_indicator_id' => true,
		'actuals' => true,
		'workplan_outputs_indicators' => true,
		'workplan' => true,
		'output' => true,
		'output_indicator' => true,
		'activities' => true,
		'responsibilities' => true,
		'risks' => true,
	];

}
