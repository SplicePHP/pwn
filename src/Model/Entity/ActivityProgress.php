<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActivityProgress Entity.
 */
class ActivityProgress extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'progress' => true,
		'value' => true,
		'actuals' => true,
	];

}
