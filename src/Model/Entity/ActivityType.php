<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActivityType Entity.
 */
class ActivityType extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'title' => true,
		'activities' => true,
		'form_activity_reports' => true,
	];

}
