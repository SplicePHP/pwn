<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity.
 */
class Location extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'parent_id' => true,
		'lft' => true,
		'rght' => true,
		'title' => true,
		'description' => true,
		'child_locations' => true,
		'form_activity_reports' => true,
		'parent_location' => true,
		'activities' => true,
	];

}
