<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WidgetParent Entity.
 */
class WidgetParent extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'title' => true,
		'widgets' => true,
	];

}
