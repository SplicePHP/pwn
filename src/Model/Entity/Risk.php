<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Risk Entity.
 */
class Risk extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'risks_and_constraints' => true,
		'risk_solution' => true,
		'workplan_outputs' => true,
	];

}
