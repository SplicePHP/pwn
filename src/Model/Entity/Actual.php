<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Actual Entity.
 */
class Actual extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'workplan_output_id' => true,
		'activity_id' => true,
		'parent_id' => true,
		'lft' => true,
		'rght' => true,
		'actual_activity' => true,
		'activity_progress_id' => true,
		'activity_status_id' => true,
		'expenditure' => true,
		'clients_new' => true,
		'clients_existing' => true,
		'referrals' => true,
		'child_actuals' => true,
		'parent_actual' => true,
		'workplan_output' => true,
		'activity' => true,
		'activity_progress' => true,
		'activity_status' => true,
		'dates' => true,
		'users' => true,
	];

}
