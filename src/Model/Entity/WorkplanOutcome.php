<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkplanOutcome Entity.
 */
class WorkplanOutcome extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'workplan_id' => true,
		'outcome_id' => true,
		'outcome_indicator_id' => true,
		'workplan_outcomes_indicators' => true,
		'workplan' => true,
		'outcome' => true,
		'outcome_indicator' => true,
	];

}
