<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OutcomeIndicator Entity.
 */
class OutcomeIndicator extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'title' => true,
		'workplan_outcomes' => true,
	];

}
