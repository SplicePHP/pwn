<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Donor Entity.
 */
class Donor extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'name' => true,
		'information' => true,
		'donor_contracts' => true,
		'workplans' => true,
		'contact_informations' => true,
	];

}
