<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Activity Entity.
 */
class Activity extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'activity' => true,
		'test' => true,
		'activity_type_id' => true,
		'target' => true,
		'budget' => true,
		'year' => true,
		'planned_quarter' => true,
		'lft' => true,
		'rght' => true,
		'parent_id' => true,
		'child_activities' => true,
		'actuals' => true,
		'sub_activities' => true,
		'parent_activity' => true,
		'activity_type' => true,
		'dates' => true,
		'inputs' => true,
		'locations' => true,
		'means_of_verifications' => true,
		'workplan_outputs' => true,
	];

}
