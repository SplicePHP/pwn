<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ImpactsIndicator Entity.
 */
class ImpactsIndicator extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'impact_id' => true,
		'indicator_id' => true,
		'impact' => true,
	];

}
