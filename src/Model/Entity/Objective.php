<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Objective Entity.
 */
class Objective extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'id' => true,
		'workplan_id' => true,
		'objective' => true,
		'objective_statements' => true,
		'workplan' => true,
		'impacts' => true,
	];

}
