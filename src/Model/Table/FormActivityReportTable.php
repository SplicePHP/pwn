<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FormActivityReportTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('form_activity_report');
		$this->primaryKey('id');

		$this->belongsTo('Locations', [
			'foreignKey' => 'location_id',
			'className' => 'Locations',
		]);
		$this->belongsTo('ActivityTypes', [
			'foreignKey' => 'activity_type_id',
			'className' => 'ActivityTypes',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('name')
			->add('date_of_activity', 'valid', ['rule' => 'datetime'])
			->allowEmpty('date_of_activity')
			->add('location_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('location_id')
			->add('activity_type_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_type_id')
			->add('number_of_people', 'valid', ['rule' => 'numeric'])
			->allowEmpty('number_of_people')
			->add('women_0-12', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_0-12')
			->add('women_13-18', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_13-18');

		return $validator;
	}
}