<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class SubActivitiesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('sub_activities');
		$this->primaryKey('id');

		$this->belongsTo('Activities', [
			'foreignKey' => 'activity_id',
			'className' => 'Activities',
		]);
		$this->belongsToMany('Dates', [
			'foreignKey' => 'sub_activity_id',
			'targetForeignKey' => 'date_id',
			'joinTable' => 'sub_activities_dates',
			'className' => 'Dates',
			'property' => 'dates',
		]);
		$this->belongsToMany('MeansOfVerifications', [
			'foreignKey' => 'sub_activity_id',
			'targetForeignKey' => 'means_of_verification_id',
			'joinTable' => 'sub_activities_means_of_verifications',
			'className' => 'MeansOfVerifications',
			'property' => 'means_of_verifications',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('activity_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_id')
			->allowEmpty('activity')
			->add('target', 'valid', ['rule' => 'decimal'])
			->allowEmpty('target');

		return $validator;
	}
}