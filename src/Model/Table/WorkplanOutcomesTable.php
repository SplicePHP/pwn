<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WorkplanOutcomesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('workplan_outcomes');
		$this->primaryKey('id');

		$this->hasMany('WorkplanOutcomesIndicators', [
			'foreignKey' => 'workplan_outcome_id',
			'className' => 'WorkplanOutcomesIndicators',
			'property' => 'workplan_outcomes_indicators',
		]);
		$this->belongsTo('Workplans', [
			'foreignKey' => 'workplan_id',
			'className' => 'Workplans',
		]);
		$this->belongsTo('Outcomes', [
			'foreignKey' => 'outcome_id',
			'className' => 'Outcomes',
		]);
		$this->belongsTo('OutcomeIndicators', [
			'foreignKey' => 'outcome_indicator_id',
			'className' => 'OutcomeIndicators',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('workplan_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('workplan_id')
			->add('outcome_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('outcome_id')
			->add('outcome_indicator_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('outcome_indicator_id');

		return $validator;
	}
}