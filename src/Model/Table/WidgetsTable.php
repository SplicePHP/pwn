<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WidgetsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('widgets');
		$this->primaryKey('id');

		$this->hasMany('WidgetAttributes', [
			'foreignKey' => 'widget_id',
			'className' => 'WidgetAttributes',
			'property' => 'widget_attributes',
		]);
		$this->belongsTo('WidgetParents', [
			'foreignKey' => 'widget_parent_id',
			'className' => 'WidgetParents',
		]);
		$this->belongsToMany('Roles', [
			'foreignKey' => 'widget_id',
			'targetForeignKey' => 'role_id',
			'joinTable' => 'widgets_roles',
			'className' => 'System.Roles',
			'property' => 'roles',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('widget_parent_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('widget_parent_id')
			->allowEmpty('alias')
			->allowEmpty('title')
			->add('type', 'valid', ['rule' => 'numeric'])
			->allowEmpty('type')
			->allowEmpty('prefix');

		return $validator;
	}
}