<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ActivitiesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('activities');
		$this->displayField('activity');
		$this->primaryKey('id');
		$this->addBehavior('Tree');

		$this->hasMany('ChildActivities', [
			'property' => 'activities',
			'foreignKey' => 'parent_id',
			'className' => 'Activities',
		]);
		$this->hasMany('Actuals', [
			'foreignKey' => 'activity_id',
			'className' => 'Actuals',
			'property' => 'actuals',
		]);
		$this->hasMany('SubActivities', [
			'foreignKey' => 'activity_id',
			'className' => 'SubActivities',
			'property' => 'sub_activities',
		]);
		$this->belongsTo('ParentActivities', [
			'foreignKey' => 'parent_id',
			'className' => 'Activities',
		]);
		$this->belongsTo('ActivityTypes', [
			'foreignKey' => 'activity_type_id',
			'className' => 'ActivityTypes',
		]);
		$this->belongsToMany('Dates', [
			'foreignKey' => 'activity_id',
			'targetForeignKey' => 'date_id',
			'joinTable' => 'activities_dates',
			'className' => 'Dates',
			'property' => 'dates',
		]);
		$this->belongsToMany('Inputs', [
			'foreignKey' => 'activity_id',
			'targetForeignKey' => 'input_id',
			'joinTable' => 'activities_inputs',
			'className' => 'Inputs',
			'property' => 'inputs',
		]);
		$this->belongsToMany('Locations', [
			'foreignKey' => 'activity_id',
			'targetForeignKey' => 'location_id',
			'joinTable' => 'activities_locations',
			'className' => 'Locations',
			'property' => 'locations',
		]);
		$this->belongsToMany('MeansOfVerifications', [
			'foreignKey' => 'activity_id',
			'targetForeignKey' => 'means_of_verification_id',
			'joinTable' => 'activities_means_of_verifications',
			'className' => 'MeansOfVerifications',
			'property' => 'means_of_verifications',
		]);
		$this->belongsToMany('WorkplanOutputs', [
			'foreignKey' => 'activity_id',
			'targetForeignKey' => 'workplan_output_id',
			'joinTable' => 'workplan_outputs_activities',
			'className' => 'WorkplanOutputs',
			'property' => 'workplan_outputs',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('activity')
			->allowEmpty('test')
			->add('activity_type_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_type_id')
			->add('target', 'valid', ['rule' => 'numeric'])
			->allowEmpty('target')
			->add('budget', 'valid', ['rule' => 'decimal'])
			->allowEmpty('budget')
			->add('year', 'valid', ['rule' => 'date'])
			->allowEmpty('year')
			->add('planned_quarter', 'valid', ['rule' => 'numeric'])
			->allowEmpty('planned_quarter')
			->add('lft', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lft')
			->add('rght', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rght')
			->add('parent_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('parent_id');

		return $validator;
	}
}