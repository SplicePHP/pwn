<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WidgetAttributesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('widget_attributes');
		$this->primaryKey('id');

		$this->belongsTo('Widgets', [
			'foreignKey' => 'widget_id',
			'className' => 'Widgets',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('widget_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('widget_id')
			->allowEmpty('title');

		return $validator;
	}
}