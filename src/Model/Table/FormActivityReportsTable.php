<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FormActivityReportsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('form_activity_reports');
		$this->primaryKey('id');

		$this->belongsTo('Locations', [
			'foreignKey' => 'location_id',
			'className' => 'Locations',
		]);
		$this->belongsTo('ActivityTypes', [
			'foreignKey' => 'activity_type_id',
			'className' => 'ActivityTypes',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('name')
			->add('date_of_activity', 'valid', ['rule' => 'datetime'])
			->allowEmpty('date_of_activity')
			->add('location_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('location_id')
			->add('activity_type_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_type_id')
			->add('number_of_people', 'valid', ['rule' => 'numeric'])
			->allowEmpty('number_of_people')
			->add('women_0_12', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_0_12')
			->add('women_13_18', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_13_18')
			->add('women_19_25', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_19_25')
			->add('women_26_35', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_26_35')
			->add('women_36_60', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_36_60')
			->add('women_61_#', 'valid', ['rule' => 'numeric'])
			->allowEmpty('women_61_#')
			->add('men_0_12', 'valid', ['rule' => 'numeric'])
			->allowEmpty('men_0_12')
			->add('men_13_18', 'valid', ['rule' => 'numeric'])
			->allowEmpty('men_13_18')
			->add('men_19_25', 'valid', ['rule' => 'numeric'])
			->allowEmpty('men_19_25')
			->add('men_26_35', 'valid', ['rule' => 'numeric'])
			->allowEmpty('men_26_35')
			->add('men_36_60', 'valid', ['rule' => 'numeric'])
			->allowEmpty('men_36_60')
			->add('men_61_#', 'valid', ['rule' => 'numeric'])
			->allowEmpty('men_61_#')
			->add('lgbti_0_12', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lgbti_0_12')
			->add('lgbti_13_18', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lgbti_13_18')
			->add('lgbti_19_25', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lgbti_19_25')
			->add('lgbti_26_35', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lgbti_26_35')
			->add('lgbti_36_60', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lgbti_36_60')
			->add('lgbti_60_#', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lgbti_60_#')
			->allowEmpty('other_ngos')
			->allowEmpty('other_partners')
			->allowEmpty('comments')
			->add('date_of_next_activity', 'valid', ['rule' => 'date'])
			->allowEmpty('date_of_next_activity')
			->add('attendance_register', 'valid', ['rule' => 'boolean'])
			->allowEmpty('attendance_register')
			->add('other_documents', 'valid', ['rule' => 'boolean'])
			->allowEmpty('other_documents');

		return $validator;
	}
}