<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MeansOfVerificationsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('means_of_verifications');
		$this->primaryKey('id');

		$this->belongsToMany('Activities', [
			'foreignKey' => 'means_of_verification_id',
			'targetForeignKey' => 'activity_id',
			'joinTable' => 'activities_means_of_verifications',
			'className' => 'Activities',
			'property' => 'activities',
		]);
		$this->belongsToMany('SubActivities', [
			'foreignKey' => 'means_of_verification_id',
			'targetForeignKey' => 'sub_activity_id',
			'joinTable' => 'sub_activities_means_of_verifications',
			'className' => 'SubActivities',
			'property' => 'sub_activities',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('title');

		return $validator;
	}
}