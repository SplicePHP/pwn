<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticlesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('articles');
		$this->primaryKey('id');
		$this->addBehavior('Timestamp');

		$this->belongsTo('Users', [
			'foreignKey' => 'user_id',
			'className' => 'System.Users',
		]);
		$this->belongsToMany('ArticleCategories', [
			'foreignKey' => 'article_id',
			'targetForeignKey' => 'article_category_id',
			'joinTable' => 'article_categories_articles',
			'className' => 'ArticleCategories',
			'property' => 'article_categories',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('user_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('user_id')
			->add('published', 'valid', ['rule' => 'boolean'])
			->allowEmpty('published')
			->allowEmpty('alias')
			->allowEmpty('title')
			->allowEmpty('caption')
			->allowEmpty('body')
			->allowEmpty('meta_description')
			->allowEmpty('meta_keys')
			->add('weight', 'valid', ['rule' => 'numeric'])
			->allowEmpty('weight');

		return $validator;
	}
}