<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WorkplanOutcomesIndicatorsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('workplan_outcomes_indicators');
		$this->primaryKey('id');

		$this->belongsTo('WorkplanOutcomes', [
			'foreignKey' => 'workplan_outcome_id',
			'className' => 'WorkplanOutcomes',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('workplan_outcome_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('workplan_outcome_id')
			->add('indicator_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('indicator_id');

		return $validator;
	}
}