<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImpactsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('impacts');
		$this->displayField('impact_statement');
		$this->primaryKey('id');
		$this->addBehavior('Tree');

		$this->hasMany('ChildImpacts', [
			'property' => 'impacts',
			'foreignKey' => 'parent_id',
			'className' => 'Impacts',
		]);
		$this->hasMany('ImpactsIndicators', [
			'foreignKey' => 'impact_id',
			'className' => 'ImpactsIndicators',
			'property' => 'impacts_indicators',
		]);
		$this->belongsTo('ParentImpacts', [
			'foreignKey' => 'parent_id',
			'className' => 'Impacts',
		]);
		$this->belongsToMany('Objectives', [
			'foreignKey' => 'impact_id',
			'targetForeignKey' => 'objective_id',
			'joinTable' => 'objectives_impacts',
			'className' => 'Objectives',
			'property' => 'objectives',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('parent_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('parent_id')
			->add('lft', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lft')
			->add('rght', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rght')
			->allowEmpty('impact_statement');

		return $validator;
	}
}