<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ActualsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('actuals');
		$this->primaryKey('id');
		$this->addBehavior('Tree');

		$this->hasMany('ChildActuals', [
			'property' => 'actuals',
			'foreignKey' => 'parent_id',
			'className' => 'Actuals',
		]);
		$this->belongsTo('ParentActuals', [
			'foreignKey' => 'parent_id',
			'className' => 'Actuals',
		]);
		$this->belongsTo('WorkplanOutputs', [
			'foreignKey' => 'workplan_output_id',
			'className' => 'WorkplanOutputs',
		]);
		$this->belongsTo('Activities', [
			'foreignKey' => 'activity_id',
			'className' => 'Activities',
		]);
		$this->belongsTo('ActivityProgresses', [
			'foreignKey' => 'activity_progress_id',
			'className' => 'ActivityProgresses',
		]);
		$this->belongsTo('ActivityStatuses', [
			'foreignKey' => 'activity_status_id',
			'className' => 'ActivityStatuses',
		]);
		$this->belongsToMany('Dates', [
			'foreignKey' => 'actual_id',
			'targetForeignKey' => 'date_id',
			'joinTable' => 'actuals_dates',
			'className' => 'Dates',
			'property' => 'dates',
		]);
		$this->belongsToMany('Users', [
			'foreignKey' => 'actual_id',
			'targetForeignKey' => 'user_id',
			'joinTable' => 'actuals_users',
			'className' => 'System.Users',
			'property' => 'users',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('workplan_output_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('workplan_output_id')
			->add('activity_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_id')
			->add('parent_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('parent_id')
			->add('lft', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lft')
			->add('rght', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rght')
			->allowEmpty('actual_activity')
			->add('activity_progress_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_progress_id')
			->add('activity_status_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('activity_status_id')
			->add('expenditure', 'valid', ['rule' => 'decimal'])
			->allowEmpty('expenditure')
			->add('clients_new', 'valid', ['rule' => 'numeric'])
			->allowEmpty('clients_new')
			->add('clients_existing', 'valid', ['rule' => 'numeric'])
			->allowEmpty('clients_existing')
			->add('referrals', 'valid', ['rule' => 'numeric'])
			->allowEmpty('referrals');

		return $validator;
	}
}