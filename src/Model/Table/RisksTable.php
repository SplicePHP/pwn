<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class RisksTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('risks');
		$this->displayField('risks_and_constraints');
		$this->primaryKey('id');

		$this->belongsToMany('WorkplanOutputs', [
			'foreignKey' => 'risk_id',
			'targetForeignKey' => 'workplan_output_id',
			'joinTable' => 'workplan_outputs_risks',
			'className' => 'WorkplanOutputs',
			'property' => 'workplan_outputs',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('risks_and_constraints')
			->allowEmpty('risk_solution');

		return $validator;
	}
}