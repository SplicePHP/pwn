<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ResponsibilitiesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('responsibilities');
		$this->primaryKey('id');

		$this->belongsToMany('Users', [
			'foreignKey' => 'responsibility_id',
			'targetForeignKey' => 'user_id',
			'joinTable' => 'responsibilities_users',
			'className' => 'System.Users',
			'property' => 'users',
		]);
		$this->belongsToMany('WorkplanOutputs', [
			'foreignKey' => 'responsibility_id',
			'targetForeignKey' => 'workplan_output_id',
			'joinTable' => 'workplan_outputs_responsibilities',
			'className' => 'WorkplanOutputs',
			'property' => 'workplan_outputs',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('title');

		return $validator;
	}
}