<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ActivityTypesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('activity_types');
		$this->primaryKey('id');

		$this->hasMany('Activities', [
			'foreignKey' => 'activity_type_id',
			'className' => 'Activities',
			'property' => 'activities',
		]);
		$this->hasMany('FormActivityReports', [
			'foreignKey' => 'activity_type_id',
			'className' => 'FormActivityReports',
			'property' => 'form_activity_reports',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('title');

		return $validator;
	}
}