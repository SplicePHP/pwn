<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WorkplanOutputsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('workplan_outputs');
		$this->primaryKey('id');

		$this->hasMany('Actuals', [
			'foreignKey' => 'workplan_output_id',
			'className' => 'Actuals',
			'property' => 'actuals',
		]);
		$this->hasMany('WorkplanOutputsIndicators', [
			'foreignKey' => 'workplan_output_id',
			'className' => 'WorkplanOutputsIndicators',
			'property' => 'workplan_outputs_indicators',
		]);
		$this->belongsTo('Workplans', [
			'foreignKey' => 'workplan_id',
			'className' => 'Workplans',
		]);
		$this->belongsTo('Outputs', [
			'foreignKey' => 'output_id',
			'className' => 'Outputs',
		]);
		$this->belongsTo('OutputIndicators', [
			'foreignKey' => 'output_indicator_id',
			'className' => 'OutputIndicators',
		]);
		$this->belongsToMany('Activities', [
			'foreignKey' => 'workplan_output_id',
			'targetForeignKey' => 'activity_id',
			'joinTable' => 'workplan_outputs_activities',
			'className' => 'Activities',
			'property' => 'activities',
		]);
		$this->belongsToMany('Responsibilities', [
			'foreignKey' => 'workplan_output_id',
			'targetForeignKey' => 'responsibility_id',
			'joinTable' => 'workplan_outputs_responsibilities',
			'className' => 'Responsibilities',
			'property' => 'responsibilities',
		]);
		$this->belongsToMany('Risks', [
			'foreignKey' => 'workplan_output_id',
			'targetForeignKey' => 'risk_id',
			'joinTable' => 'workplan_outputs_risks',
			'className' => 'Risks',
			'property' => 'risks',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('workplan_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('workplan_id')
			->add('output_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('output_id')
			->add('output_indicator_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('output_indicator_id');

		return $validator;
	}
}