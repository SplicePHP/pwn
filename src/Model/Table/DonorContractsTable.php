<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DonorContractsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('donor_contracts');
		$this->primaryKey('id');

		$this->belongsTo('ContractTypes', [
			'foreignKey' => 'contract_type_id',
			'className' => 'ContractTypes',
		]);
		$this->belongsTo('Donors', [
			'foreignKey' => 'donor_id',
			'className' => 'Donors',
		]);
		$this->belongsToMany('Attachments', [
			'foreignKey' => 'donor_contract_id',
			'targetForeignKey' => 'attachment_id',
			'joinTable' => 'donor_contracts_attachments',
			'className' => 'Attachments',
			'property' => 'attachments',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('contract_type_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('contract_type_id')
			->add('donor_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('donor_id')
			->add('total_amount', 'valid', ['rule' => 'decimal'])
			->allowEmpty('total_amount')
			->add('start_date', 'valid', ['rule' => 'date'])
			->allowEmpty('start_date')
			->add('end_date', 'valid', ['rule' => 'date'])
			->allowEmpty('end_date')
			->allowEmpty('other');

		return $validator;
	}
}