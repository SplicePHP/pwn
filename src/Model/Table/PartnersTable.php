<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PartnersTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('partners');
		$this->displayField('partner');
		$this->primaryKey('id');

		$this->belongsToMany('Workplans', [
			'foreignKey' => 'partner_id',
			'targetForeignKey' => 'workplan_id',
			'joinTable' => 'workplans_partners',
			'className' => 'Workplans',
			'property' => 'workplans',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('partner')
			->allowEmpty('description');

		return $validator;
	}
}