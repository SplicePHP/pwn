<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DonorsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('donors');
		$this->primaryKey('id');

		$this->hasMany('DonorContracts', [
			'foreignKey' => 'donor_id',
			'className' => 'DonorContracts',
			'property' => 'donor_contracts',
		]);
		$this->hasMany('Workplans', [
			'foreignKey' => 'donor_id',
			'className' => 'Workplans',
			'property' => 'workplans',
		]);
		$this->belongsToMany('ContactInformations', [
			'foreignKey' => 'donor_id',
			'targetForeignKey' => 'contact_information_id',
			'joinTable' => 'donors_contact_informations',
			'className' => 'ContactInformations',
			'property' => 'contact_informations',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('name')
			->allowEmpty('information');

		return $validator;
	}
}