<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ActivityProgressesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('activity_progresses');
		$this->displayField('progress');
		$this->primaryKey('id');

		$this->hasMany('Actuals', [
			'foreignKey' => 'activity_progress_id',
			'className' => 'Actuals',
			'property' => 'actuals',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('progress')
			->add('value', 'valid', ['rule' => 'numeric'])
			->allowEmpty('value');

		return $validator;
	}
}