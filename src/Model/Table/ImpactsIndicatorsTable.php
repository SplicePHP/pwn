<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImpactsIndicatorsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('impacts_indicators');
		$this->primaryKey('id');

		$this->belongsTo('Impacts', [
			'foreignKey' => 'impact_id',
			'className' => 'Impacts',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('impact_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('impact_id')
			->add('indicator_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('indicator_id');

		return $validator;
	}
}