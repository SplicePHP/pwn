<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class LocationsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('locations');
		$this->primaryKey('id');
		$this->addBehavior('Tree');

		$this->hasMany('ChildLocations', [
			'property' => 'locations',
			'foreignKey' => 'parent_id',
			'className' => 'Locations',
		]);
		$this->hasMany('FormActivityReports', [
			'foreignKey' => 'location_id',
			'className' => 'FormActivityReports',
			'property' => 'form_activity_reports',
		]);
		$this->belongsTo('ParentLocations', [
			'foreignKey' => 'parent_id',
			'className' => 'Locations',
		]);
		$this->belongsToMany('Activities', [
			'foreignKey' => 'location_id',
			'targetForeignKey' => 'activity_id',
			'joinTable' => 'activities_locations',
			'className' => 'Activities',
			'property' => 'activities',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('parent_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('parent_id')
			->add('lft', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lft')
			->add('rght', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rght')
			->allowEmpty('title')
			->allowEmpty('description');

		return $validator;
	}
}