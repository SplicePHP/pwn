<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ObjectivesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('objectives');
		$this->displayField('objective');
		$this->primaryKey('id');

		$this->hasMany('ObjectiveStatements', [
			'foreignKey' => 'objective_id',
			'className' => 'ObjectiveStatements',
			'property' => 'objective_statements',
		]);
		$this->belongsTo('Workplans', [
			'foreignKey' => 'workplan_id',
			'className' => 'Workplans',
		]);
		$this->belongsToMany('Impacts', [
			'foreignKey' => 'objective_id',
			'targetForeignKey' => 'impact_id',
			'joinTable' => 'objectives_impacts',
			'className' => 'Impacts',
			'property' => 'impacts',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('workplan_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('workplan_id')
			->allowEmpty('objective');

		return $validator;
	}
}