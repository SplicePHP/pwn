<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticleCategoriesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('article_categories');
		$this->primaryKey('id');
		$this->addBehavior('Timestamp');
		$this->addBehavior('Tree');

		$this->hasMany('ChildArticleCategories', [
			'property' => 'article_categories',
			'foreignKey' => 'parent_id',
			'className' => 'ArticleCategories',
		]);
		$this->belongsTo('ParentArticleCategories', [
			'foreignKey' => 'parent_id',
			'className' => 'ArticleCategories',
		]);
		$this->belongsToMany('Articles', [
			'foreignKey' => 'article_category_id',
			'targetForeignKey' => 'article_id',
			'joinTable' => 'article_categories_articles',
			'className' => 'Articles',
			'property' => 'articles',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('alias')
			->allowEmpty('title')
			->allowEmpty('description')
			->add('parent_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('parent_id')
			->add('lft', 'valid', ['rule' => 'numeric'])
			->allowEmpty('lft')
			->add('rght', 'valid', ['rule' => 'numeric'])
			->allowEmpty('rght');

		return $validator;
	}
}