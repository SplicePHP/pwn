<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class WorkplansTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('workplans');
		$this->primaryKey('id');

		$this->hasMany('Objectives', [
			'foreignKey' => 'workplan_id',
			'className' => 'Objectives',
			'property' => 'objectives',
		]);
		$this->hasMany('WorkplanOutcomes', [
			'foreignKey' => 'workplan_id',
			'className' => 'WorkplanOutcomes',
			'property' => 'workplan_outcomes',
		]);
		$this->hasMany('WorkplanOutputs', [
			'foreignKey' => 'workplan_id',
			'className' => 'WorkplanOutputs',
			'property' => 'workplan_outputs',
		]);
		$this->belongsTo('Donors', [
			'foreignKey' => 'donor_id',
			'className' => 'Donors',
		]);
		$this->belongsToMany('Partners', [
			'foreignKey' => 'workplan_id',
			'targetForeignKey' => 'partner_id',
			'joinTable' => 'workplans_partners',
			'className' => 'Partners',
			'property' => 'partners',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('donor_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('donor_id');

		return $validator;
	}
}