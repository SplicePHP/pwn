<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ObjectiveStatementsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('objective_statements');
		$this->primaryKey('id');

		$this->belongsTo('Objectives', [
			'foreignKey' => 'objective_id',
			'className' => 'Objectives',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('objective_id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('objective_id')
			->allowEmpty('statement');

		return $validator;
	}
}