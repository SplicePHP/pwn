<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class OutputsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('outputs');
		$this->displayField('title');
		$this->primaryKey('id');

		$this->hasMany('WorkplanOutputs', [
			'foreignKey' => 'output_id',
			'className' => 'WorkplanOutputs',
			'property' => 'workplan_outputs',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('title');

		return $validator;
	}
}