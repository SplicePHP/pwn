<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class DatesTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('dates');
		$this->primaryKey('id');

		$this->belongsToMany('Activities', [
			'foreignKey' => 'date_id',
			'targetForeignKey' => 'activity_id',
			'joinTable' => 'activities_dates',
			'className' => 'Activities',
			'property' => 'activities',
		]);
		$this->belongsToMany('Actuals', [
			'foreignKey' => 'date_id',
			'targetForeignKey' => 'actual_id',
			'joinTable' => 'actuals_dates',
			'className' => 'Actuals',
			'property' => 'actuals',
		]);
		$this->belongsToMany('SubActivities', [
			'foreignKey' => 'date_id',
			'targetForeignKey' => 'sub_activity_id',
			'joinTable' => 'sub_activities_dates',
			'className' => 'SubActivities',
			'property' => 'sub_activities',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->add('begin', 'valid', ['rule' => 'date'])
			->allowEmpty('begin')
			->add('end', 'valid', ['rule' => 'date'])
			->allowEmpty('end')
			->allowEmpty('info');

		return $validator;
	}
}