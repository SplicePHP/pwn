<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ContactInformationsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('contact_informations');
		$this->primaryKey('id');

		$this->belongsToMany('Donors', [
			'foreignKey' => 'contact_information_id',
			'targetForeignKey' => 'donor_id',
			'joinTable' => 'donors_contact_informations',
			'className' => 'Donors',
			'property' => 'donors',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('alias')
			->allowEmpty('name')
			->allowEmpty('surname')
			->allowEmpty('tel')
			->allowEmpty('mobile')
			->allowEmpty('fax')
			->allowEmpty('street_address')
			->allowEmpty('postal_address');

		return $validator;
	}
}