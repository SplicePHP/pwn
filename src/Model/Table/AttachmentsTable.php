<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AttachmentsTable extends Table {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('attachments');
		$this->primaryKey('id');

		$this->belongsToMany('DonorContracts', [
			'foreignKey' => 'attachment_id',
			'targetForeignKey' => 'donor_contract_id',
			'joinTable' => 'donor_contracts_attachments',
			'className' => 'DonorContracts',
			'property' => 'donor_contracts',
		]);
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->allowEmpty('title');

		return $validator;
	}
}