<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('activity');
	echo $this->Form->input('test');
	echo $this->Form->input('activity_type_id', array('alias'=>'ActivityTypes',));
	echo $this->Form->input('target');
	echo $this->Form->input('budget');
	echo $this->Form->input('year');
	echo $this->Form->input('planned_quarter');
	echo $this->Form->input('parent_id', array('alias'=>'ParentActivities',));
	echo $this->Form->input('dates._ids', array('alias'=>'Dates',));
	echo $this->Form->input('inputs._ids', array('alias'=>'Inputs',));
	echo $this->Form->input('locations._ids', array('alias'=>'Locations',));
	echo $this->Form->input('means_of_verifications._ids', array('alias'=>'MeansOfVerifications',));
	echo $this->Form->input('workplan_outputs._ids', array('alias'=>'WorkplanOutputs',));
$content[__('Activity')] = _end();
$content['ChildActivities'] = $this->Form->hasMany('ChildActivities',['hidden'=>['parent_id'=>$entity->id]]);
$content['Actuals'] = $this->Form->hasMany('Actuals',['hidden'=>['activity_id'=>$entity->id]]);
$content['SubActivities'] = $this->Form->hasMany('SubActivities',['hidden'=>['activity_id'=>$entity->id]]);
$content['ParentActivity'] = $this->Form->belongsTo('ParentActivities');
$content['ActivityType'] = $this->Form->belongsTo('ActivityTypes');
$content['Dates'] = $this->Form->belongsToMany('Dates',['exclude'=>['activities']]);
$content['Inputs'] = $this->Form->belongsToMany('Inputs',['exclude'=>['activities']]);
$content['Locations'] = $this->Form->belongsToMany('Locations',['exclude'=>['activities']]);
$content['MeansOfVerifications'] = $this->Form->belongsToMany('MeansOfVerifications',['exclude'=>['activities']]);
$content['WorkplanOutputs'] = $this->Form->belongsToMany('WorkplanOutputs',['exclude'=>['activities']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();