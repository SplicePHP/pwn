<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('parent_id', array('alias'=>'ParentLocations',));
	echo $this->Form->input('title');
	echo $this->Form->input('description');
	echo $this->Form->input('activities._ids', array('alias'=>'Activities',));
$content[__('Location')] = _end();
$content['ChildLocations'] = $this->Form->hasMany('ChildLocations',['hidden'=>['parent_id'=>$entity->id]]);
$content['FormActivityReports'] = $this->Form->hasMany('FormActivityReports',['hidden'=>['location_id'=>$entity->id]]);
$content['ParentLocation'] = $this->Form->belongsTo('ParentLocations');
$content['Activities'] = $this->Form->belongsToMany('Activities',['exclude'=>['locations']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();