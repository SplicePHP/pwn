<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('donor_id', array('alias'=>'Donors',));
	echo $this->Form->input('partners._ids', array('alias'=>'Partners',));
$content[__('Workplan')] = _end();
$content['Objectives'] = $this->Form->hasMany('Objectives',['hidden'=>['workplan_id'=>$entity->id]]);
$content['WorkplanOutcomes'] = $this->Form->hasMany('WorkplanOutcomes',['hidden'=>['workplan_id'=>$entity->id]]);
$content['WorkplanOutputs'] = $this->Form->hasMany('WorkplanOutputs',['hidden'=>['workplan_id'=>$entity->id]]);
$content['Donor'] = $this->Form->belongsTo('Donors');
$content['Partners'] = $this->Form->belongsToMany('Partners',['exclude'=>['workplans']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();