<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('workplan_id', array('alias'=>'Workplans',));
	echo $this->Form->input('objective');
	echo $this->Form->input('impacts._ids', array('alias'=>'Impacts',));
$content[__('Objective')] = _end();
$content['ObjectiveStatements'] = $this->Form->hasMany('ObjectiveStatements',['hidden'=>['objective_id'=>$entity->id]]);
$content['Workplan'] = $this->Form->belongsTo('Workplans');
$content['Impacts'] = $this->Form->belongsToMany('Impacts',['exclude'=>['objectives']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();