<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('title');
	echo $this->Form->input('activities._ids', array('alias'=>'Activities',));
	echo $this->Form->input('sub_activities._ids', array('alias'=>'SubActivities',));
$content[__('MeansOfVerification')] = _end();
$content['Activities'] = $this->Form->belongsToMany('Activities',['exclude'=>['means_of_verifications']]);
$content['SubActivities'] = $this->Form->belongsToMany('SubActivities',['exclude'=>['means_of_verifications']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();