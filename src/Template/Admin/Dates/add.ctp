<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('begin');
	echo $this->Form->input('end');
	echo $this->Form->input('info');
	echo $this->Form->input('activities._ids', array('alias'=>'Activities',));
	echo $this->Form->input('actuals._ids', array('alias'=>'Actuals',));
	echo $this->Form->input('sub_activities._ids', array('alias'=>'SubActivities',));
$content[__('Date')] = _end();
$content['Activities'] = $this->Form->belongsToMany('Activities',['exclude'=>['dates']]);
$content['Actuals'] = $this->Form->belongsToMany('Actuals',['exclude'=>['dates']]);
$content['SubActivities'] = $this->Form->belongsToMany('SubActivities',['exclude'=>['dates']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();