<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('alias');
	echo $this->Form->input('name');
	echo $this->Form->input('surname');
	echo $this->Form->input('tel');
	echo $this->Form->input('mobile');
	echo $this->Form->input('fax');
	echo $this->Form->input('street_address');
	echo $this->Form->input('postal_address');
	echo $this->Form->input('donors._ids', array('alias'=>'Donors',));
$content[__('ContactInformation')] = _end();
$content['Donors'] = $this->Form->belongsToMany('Donors',['exclude'=>['contact_informations']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();