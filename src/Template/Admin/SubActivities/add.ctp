<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('activity_id', array('alias'=>'Activities',));
	echo $this->Form->input('activity');
	echo $this->Form->input('target');
	echo $this->Form->input('dates._ids', array('alias'=>'Dates',));
	echo $this->Form->input('means_of_verifications._ids', array('alias'=>'MeansOfVerifications',));
$content[__('SubActivity')] = _end();
$content['Activity'] = $this->Form->belongsTo('Activities');
$content['Dates'] = $this->Form->belongsToMany('Dates',['exclude'=>['sub_activities']]);
$content['MeansOfVerifications'] = $this->Form->belongsToMany('MeansOfVerifications',['exclude'=>['sub_activities']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();