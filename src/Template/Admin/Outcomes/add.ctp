<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('title');
$content[__('Outcome')] = _end();
$content['WorkplanOutcomes'] = $this->Form->hasMany('WorkplanOutcomes',['hidden'=>['outcome_id'=>$entity->id]]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();