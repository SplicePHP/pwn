<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('name');
	echo $this->Form->input('date_of_activity');
	echo $this->Form->input('location_id', array('alias'=>'Locations',));
	echo $this->Form->input('activity_type_id', array('alias'=>'ActivityTypes',));
	echo $this->Form->input('number_of_people');
	echo $this->Form->input('women_0_12');
	echo $this->Form->input('women_13_18');
	echo $this->Form->input('women_19_25');
	echo $this->Form->input('women_26_35');
	echo $this->Form->input('women_36_60');
	echo $this->Form->input('women_61_#');
	echo $this->Form->input('men_0_12');
	echo $this->Form->input('men_13_18');
	echo $this->Form->input('men_19_25');
	echo $this->Form->input('men_26_35');
	echo $this->Form->input('men_36_60');
	echo $this->Form->input('men_61_#');
	echo $this->Form->input('lgbti_0_12');
	echo $this->Form->input('lgbti_13_18');
	echo $this->Form->input('lgbti_19_25');
	echo $this->Form->input('lgbti_26_35');
	echo $this->Form->input('lgbti_36_60');
	echo $this->Form->input('lgbti_60_#');
	echo $this->Form->input('other_ngos');
	echo $this->Form->input('other_partners');
	echo $this->Form->input('comments');
	echo $this->Form->input('date_of_next_activity');
	echo $this->Form->input('attendance_register');
	echo $this->Form->input('other_documents');
$content[__('FormActivityReport')] = _end();
$content['Location'] = $this->Form->belongsTo('Locations');
$content['ActivityType'] = $this->Form->belongsTo('ActivityTypes');
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();