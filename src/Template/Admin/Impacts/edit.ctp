<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('parent_id', array('alias'=>'ParentImpacts',));
	echo $this->Form->input('impact_statement');
	echo $this->Form->input('objectives._ids', array('alias'=>'Objectives',));
$content[__('Impact')] = _end();
$content['ChildImpacts'] = $this->Form->hasMany('ChildImpacts',['hidden'=>['parent_id'=>$entity->id]]);
$content['ImpactsIndicators'] = $this->Form->hasMany('ImpactsIndicators',['hidden'=>['impact_id'=>$entity->id]]);
$content['ParentImpact'] = $this->Form->belongsTo('ParentImpacts');
$content['Objectives'] = $this->Form->belongsToMany('Objectives',['exclude'=>['impacts']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();