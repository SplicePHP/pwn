<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('name');
	echo $this->Form->input('date_of_activity');
	echo $this->Form->input('location_id', array('alias'=>'Locations',));
	echo $this->Form->input('activity_type_id', array('alias'=>'ActivityTypes',));
	echo $this->Form->input('number_of_people');
	echo $this->Form->input('women_0-12');
	echo $this->Form->input('women_13-18');
$content[__('FormActivityReport')] = _end();
$content['Location'] = $this->Form->belongsTo('Locations');
$content['ActivityType'] = $this->Form->belongsTo('ActivityTypes');
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();