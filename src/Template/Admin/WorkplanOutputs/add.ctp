<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('workplan_id', array('alias'=>'Workplans',));
	echo $this->Form->input('output_id', array('alias'=>'Outputs',));
	echo $this->Form->input('output_indicator_id', array('alias'=>'OutputIndicators',));
	echo $this->Form->input('activities._ids', array('alias'=>'Activities',));
	echo $this->Form->input('responsibilities._ids', array('alias'=>'Responsibilities',));
	echo $this->Form->input('risks._ids', array('alias'=>'Risks',));
$content[__('WorkplanOutput')] = _end();
$content['Actuals'] = $this->Form->hasMany('Actuals',['hidden'=>['workplan_output_id'=>$entity->id]]);
$content['WorkplanOutputsIndicators'] = $this->Form->hasMany('WorkplanOutputsIndicators',['hidden'=>['workplan_output_id'=>$entity->id]]);
$content['Workplan'] = $this->Form->belongsTo('Workplans');
$content['Output'] = $this->Form->belongsTo('Outputs');
$content['OutputIndicator'] = $this->Form->belongsTo('OutputIndicators');
$content['Activities'] = $this->Form->belongsToMany('Activities',['exclude'=>['workplan_outputs']]);
$content['Responsibilities'] = $this->Form->belongsToMany('Responsibilities',['exclude'=>['workplan_outputs']]);
$content['Risks'] = $this->Form->belongsToMany('Risks',['exclude'=>['workplan_outputs']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();