<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('contract_type_id', array('alias'=>'ContractTypes',));
	echo $this->Form->input('donor_id', array('alias'=>'Donors',));
	echo $this->Form->input('total_amount');
	echo $this->Form->input('start_date');
	echo $this->Form->input('end_date');
	echo $this->Form->input('other');
	echo $this->Form->input('attachments._ids', array('alias'=>'Attachments',));
$content[__('DonorContract')] = _end();
$content['ContractType'] = $this->Form->belongsTo('ContractTypes');
$content['Donor'] = $this->Form->belongsTo('Donors');
$content['Attachments'] = $this->Form->belongsToMany('Attachments',['exclude'=>['donor_contracts']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();