<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('user_id', array('alias'=>'Users',));
	echo $this->Form->input('published');
	echo $this->Form->input('alias');
	echo $this->Form->input('title');
	echo $this->Form->input('caption');
	echo $this->Form->input('body');
	echo $this->Form->input('meta_description');
	echo $this->Form->input('meta_keys');
	echo $this->Form->input('article_categories._ids', array('alias'=>'ArticleCategories',));
$content[__('Article')] = _end();
$content['User'] = $this->Form->belongsTo('Users');
$content['ArticleCategories'] = $this->Form->belongsToMany('ArticleCategories',['exclude'=>['articles']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();