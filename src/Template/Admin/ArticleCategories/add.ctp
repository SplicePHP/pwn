<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('alias');
	echo $this->Form->input('title');
	echo $this->Form->input('description');
	echo $this->Form->input('parent_id', array('alias'=>'ParentArticleCategories',));
	echo $this->Form->input('articles._ids', array('alias'=>'Articles',));
$content[__('ArticleCategory')] = _end();
$content['ChildArticleCategories'] = $this->Form->hasMany('ChildArticleCategories',['hidden'=>['parent_id'=>$entity->id]]);
$content['ParentArticleCategory'] = $this->Form->belongsTo('ParentArticleCategories');
$content['Articles'] = $this->Form->belongsToMany('Articles',['exclude'=>['article_categories']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();