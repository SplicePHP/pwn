<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('workplan_output_id', array('alias'=>'WorkplanOutputs',));
	echo $this->Form->input('activity_id', array('alias'=>'Activities',));
	echo $this->Form->input('parent_id', array('alias'=>'ParentActuals',));
	echo $this->Form->input('actual_activity');
	echo $this->Form->input('activity_progress_id', array('alias'=>'ActivityProgresses',));
	echo $this->Form->input('activity_status_id', array('alias'=>'ActivityStatuses',));
	echo $this->Form->input('expenditure');
	echo $this->Form->input('clients_new');
	echo $this->Form->input('clients_existing');
	echo $this->Form->input('referrals');
	echo $this->Form->input('dates._ids', array('alias'=>'Dates',));
	echo $this->Form->input('users._ids', array('alias'=>'Users',));
$content[__('Actual')] = _end();
$content['ChildActuals'] = $this->Form->hasMany('ChildActuals',['hidden'=>['parent_id'=>$entity->id]]);
$content['ParentActual'] = $this->Form->belongsTo('ParentActuals');
$content['WorkplanOutput'] = $this->Form->belongsTo('WorkplanOutputs');
$content['Activity'] = $this->Form->belongsTo('Activities');
$content['ActivityProgress'] = $this->Form->belongsTo('ActivityProgresses');
$content['ActivityStatus'] = $this->Form->belongsTo('ActivityStatuses');
$content['Dates'] = $this->Form->belongsToMany('Dates',['exclude'=>['actuals']]);
$content['Users'] = $this->Form->belongsToMany('Users',['exclude'=>['actuals']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();