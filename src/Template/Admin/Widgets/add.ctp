<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('widget_parent_id', array('alias'=>'WidgetParents',));
	echo $this->Form->input('alias');
	echo $this->Form->input('title');
	echo $this->Form->input('type');
	echo $this->Form->input('prefix');
	echo $this->Form->input('roles._ids', array('alias'=>'Roles',));
$content[__('Widget')] = _end();
$content['WidgetAttributes'] = $this->Form->hasMany('WidgetAttributes',['hidden'=>['widget_id'=>$entity->id]]);
$content['WidgetParent'] = $this->Form->belongsTo('WidgetParents');
$content['Roles'] = $this->Form->belongsToMany('Roles',['exclude'=>['widgets']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('save_new');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();