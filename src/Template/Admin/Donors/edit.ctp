<?php
/* @var $this \System\View\View */
$this->extend(rget('config;content.extend', '/Common/edit'));
$ui = $this->Ui;
$context = Splice\Cake\Context::load();


$form = $this->Form->create($entity);

if(!$context->is('child') && !$context->is('request')){
    $this->start('form-start');
    echo $form;
    $this->end();
}

_start();

//echo $context->debug();
	echo $this->Form->input('id');
	echo $this->Form->input('name');
	echo $this->Form->input('information');
	echo $this->Form->input('contact_informations._ids', array('alias'=>'ContactInformations',));
$content[__('Donor')] = _end();
$content['DonorContracts'] = $this->Form->hasMany('DonorContracts',['hidden'=>['donor_id'=>$entity->id]]);
$content['Workplans'] = $this->Form->hasMany('Workplans',['hidden'=>['donor_id'=>$entity->id]]);
$content['ContactInformations'] = $this->Form->belongsToMany('ContactInformations',['exclude'=>['donors']]);
    
echo $ui->content($content);
    
$this->start('actions');
echo $this->Form->action('apply');
echo $this->Form->action('done');
echo $this->Form->action('cancel');
$this->end();

$this->start('form-end');
echo $this->Form->end();
$this->end();