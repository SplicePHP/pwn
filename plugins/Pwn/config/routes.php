<?php
use Cake\Routing\Router;

Router::plugin('Pwn', function ($routes) {
    $routes->prefix('admin', function ($routes) {
        $routes->connect('/:controller', ['action' => 'index']);
        $routes->connect('/:controller/:action/*');
    });
    $routes->connect('/:controller', ['action' => 'index']);
    $routes->connect('/:controller/:action/*');
    $routes->fallbacks();
});